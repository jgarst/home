class ResumeController < ApplicationController
  impressionist actions: [:pdf]

  def pdf
    publicResume = Document.find_by(title: "Resume-Jared")
    impressionist(publicResume)
    send_data(Paperclip.io_adapters.for(publicResume.pdf).read, 
              disposition:'inline',
              filename: publicResume.pdf_file_name,
              type: publicResume.pdf_content_type)
  end

end
