class Document < ActiveRecord::Base
  has_attached_file :pdf
  validates_attachment :pdf, content_type: {content_type: "application/pdf"}
  validates_with AttachmentPresenceValidator, :attributes => :pdf
  is_impressionable
end
