require 'open-uri'

@resumePath = Rails.root.join('app', 'assets', 'pdfs', 'PublicResume.pdf')
@resumeUrl = "https://bitbucket.org/jgarst/resume/raw/master/Latex/" \
             "finalResumes/PublicResume.pdf"


desc "Update the database with the resume"
task :createResumeDB => :environment do
  File.open(@resumePath, "rb") do |resumeFile|
    @resumeRecord = Document.find_by(title: "Resume-Jared")
    if @resumeRecord.nil?
      @resumeRecord = Document.new(title: "Resume-Jared", pdf: resumeFile)
    else
      @resumeRecord.pdf = resumeFile
    end
  end
  @resumeRecord.save
end

desc "Grab resume from bitbucket and save it locally"
task :fetchResume do 
  File.open(@resumeFile, "wb") do |resume|
    open(@resumeUrl, "rb") do |remote_resume|
      resume.write(remote_resume.read)
    end
  end
end
